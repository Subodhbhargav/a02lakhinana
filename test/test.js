QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});
QUnit.test('Testing  number of what percentage in the given number ', function (assert) {
    // 
    assert.equal(test(30,30), 100,  "works with positive integers");
    
});
QUnit.test('Testing  number of what percentage in the given number ', function (assert) {
assert.equal(test(-10,-10), 100, "works with a negative number");
});
QUnit.test('Testing  number of what percentage in the given number ', function (assert) {
    assert.throws(function() { test(NaN) }, 'Given is not a number');
});
QUnit.test('Testing  number of what percentage in the given number ', function (assert) {
    assert.throws(function() { test(null) }, 'Given is not a number');
});
QUnit.test('Testing  number of what percentage in the given number ', function (assert) {
    assert.throws(function() { test("abc") }, 'Given is not a number');
});
QUnit.test('Testing  number of what percentage in the given number ', function (assert) {
    assert.throws(function() { test(undefined) }, 'Given is not a number');
});